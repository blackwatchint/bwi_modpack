## Introduction
This repository is a simple tracker for the [Arma 3](https://arma3.com/) mods used by [Black Watch International](http://blackwatch-int.com).

## Modpack Criteria
Each mod in the Black Watch International modpack repository is required to meet the following criteria:

1. Must either be a standalone mod, or have one fully approved dependency.
2. Must be actively maintained by the original author or group.
3. Must not contain frequent script or RPT errors.
4. Must serve a clear and useful purpose.

Exceptions may be made only when at least three of the criteria are met.

## Current Modpack
Following is a list of all mods currently in the modpack, organised by the folder that contains them:

- @Core_ACE
  - ACE (v3.14.1)
- @Core_ACRE
  - ACRE2 (v2.8.0.1037)
- @Core_BWI
  - BWI Addons Core (03MAY2021)
- @Core_CBA
  - CBA (v3.15.6.211004)
- @Core_CUP
  - CUP Terrains Core (v1.16.1)
  - CUP Terrains Maps (v1.16.0)
- @Core_Misc
  - VCOM AI (v3.4.0)
  - BackpackOnChest (v1.2.0)
  - DUI (v1.9.3)
  - FEMAL3 (v2.0)
  - GRAD Trenches (v1.6.8.9)
- @Main_BAF
  - 3CB BAF Units (v4.10)
  - 3CB BAF Weapons (v3.0)
  - 3CB BAF Equipment (v2.6)
  - 3CB BAF Vehicles (v10.0)
  - 3CB BAF Factions (v5.0.3)
- @Main_BWA
  - BWMod (v2.3)
- @Main_BWI
  - BWI Addons Main (03MAY2021)
- @Main_FFAA
  - FFAA (v6.1.7)
- @Main_FIR
  - FIR AWS (v3.56)
  - A-10 Warthog (v1.83a)
  - EF-2000 (v7.5)
  - F-14 Tomcat (v1.99)
  - F-15 Eagle (v1.62)
  - F-16 Fighting Falcon (v1.986)
  - F-35B Project Lightning (v1.2)
  - AV-8B Harrier (v0.62)
  - SU-25SM3 Grach (v0.16)
  - F/A-18 Super Hornet (v5.1)
  - Su-35S Flanker E (v3.0.0)
- @Main_JSRS
  - JSRS Soundmod (v20.0810)
- @Main_Maps
  - Anizay (v0.08)
  - Bozcaada (v1.2)
  - Djalka (12FEB2020)
  - Fallujah (v1.2)
  - Fapovo Island (v1.75)
  - FFAA Gallaecia (Beta3)
  - GOS Al Rayak (v0044)
  - GOS Gunkizli (v0.52)
  - GOS N'ziwasogo (v2.3.1)
  - Hellanmaa (v1.03)
  - Isla Abramia (v3.9)
  - Isla Duala (v3.9.5)
  - Island Panthera (v3.91)
  - Kujari (v0.04)
  - Lingor (v3.9.6.01)
  - Pulau (v0.05)
  - Rosche (v2.0)
  - Ruha (v0.12)
  - Suursaari (v0.02)
  - Uzbin Valley (31MAR2021)
  - Vinjesvingen (v1.21)
  - Virolahti (v1.01)
- @Main_Misc
  - RKSL Studios Attachments (v3.00)
  - Interiors for CUP (v.0.0.4)
  - AWESome (v1.3.10)
- @Main_Pook
  - Pook Soviet Air Force Pack (24MAY2020)
- @Main_RHS
  - RHS: USAF (v0.5.6)
  - RHS: AFRF (v0.5.6)
  - RHS: GREF (v0.5.6)
  - RHS: SAF  (v0.5.6)
- @Main_RnT
  - Redd 'n' Tank Vehicles (v1.12.173)
- @Main_TAC
  - Theseus Services (v1.19.0)
- @Main_USAF
  - USAF Mod - Main (1.0.6.2)
  - USAF Mod - Fighter (1.0.6.2)
  - USAF Mod - Utility (1.0.6.2)

## Mod Suggestions
New mod suggestions can be made by [creating a new issue](https://gitlab.com/blackwatchint/bwi_modpack/issues) with the following information:

- Brief description of the mod.
- Why you believe the mod would be a good addition.
- Whether you tested the mod in singleplayer or multiplayer.
- Link to the RPT file from when you tested the mod. [How do I find my RPT file?](https://community.bistudio.com/wiki/Crash_Files#Arma_3)
- Link to the mod's download location (preferably Steam Workshop).

Suggestions that contain all of this information will go through the following process before being included in the modpack:

1. Checked against the criteria for inclusion.
2. Discussed in a meeting for approval.
3. Formally tested for inclusion.

## Downloading
The modpack is available only to members of Black Watch International, via our Arma3Sync repository. Details on downloading the modpack can be found on our [website](http://blackwatch-int.com/field-manual/appendix/modset-repository-instructions).

## Reporting Issues
Issues with the modpack may be reported to our [issue tracker](https://gitlab.com/blackwatchint/bwi_modpack/issues). Please make sure to include the following information:

- Description of the issue.
- Steps to reproduce.
- Where the issue occurred (singleplayer/local multiplayer/dedicated server).
- The template or mission that the issue occurred on.
- Link to the RPT file if a crash or script error occurred. [How do I find my RPT file?](https://community.bistudio.com/wiki/Crash_Files#Arma_3)

Please be aware that most issues will be caused by a specific mod, and should instead be reported directly to the respective mod's issue tracker. 

## Licensing
All mods are the property of their respective owners. Please refer to any documentation included with each mod for license details.
