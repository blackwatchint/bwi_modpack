**29MAY2022**
- Update GRAD Trenches to v1.6.8.9
- Update FFAA Mod v6.1.7
- Update RHSSAF v0.5.6
- Update RHSGREF v0.5.6
- Update RHSUSAF v0.5.6


**09MAR2022**
- Update ACE to 3.14.1
- Update 3CB Factions v5.0.3
- Update DUI-Squad Radar v1.9.3


**07NOV2021**
- Update Uzbin Valley to 31MAR2021
- Update ACE to v3.14.0
- Update CBA to CBA_A3 v3.15.6.211004
- Update RHSAFRF v0.5.6
- Add GRAD trenches


**06OCT2021**
- Update Theseus Services to v1.19.0
- Update ACE to v3.13.6
- Update Fapovo Island v1.75
- Update CBA to v3.15.3.210310
- Update BWMod to v2.3
- Add Interiors for CUP
- Add AWESome: Aerial Warfare Enhanced Somehow
- Add Pook Soviet Air Force Pack
- Add USAF Mod Complete
- Add FFAA Mod


**01MAR2021**
- Updated FIR AWS to v3.56
- Updated 3CB Factions to v4.0.3
- Updated Theseus Services to v1.18.0
- Removed STUI
- Added DUI v1.8.0
- Added G.O.S. N'ziwasogo v2.3.1
- Added FFAA Gallaecia Beta3
- Added Fapovo Island v1.6
- Added Uzbin Valley 01SEP2020
- Updated AV-8B Harrier 2 to v0.62
- Added Djalka 12FEB2020


**25JAN2021**
- Updated F-15 Eagle to v1.62
- Updated FIR AWS to v3.55
- Updated Island Panthera to v3.91


**28DEC2020**
- Updated BWI Addons to 28DEC2020
- Updated Redd'n'Tank Vehicles to v1.12.173
- Updated F-15 Eagle to v1.61
- Updated CUP Terrains - Maps to v1.16.0
- Updated CUP Terrains - Core to v1.16.1
- Updated ACRE2 to 2.8.0.1037
- Updated Theseus Services to v1.17.0
- Updated 3CB Factions to v3.0.2
- Updated ACEX to 3.5.4
- Updated ACE to v3.13.5
- Updated Su-25SM3 Grach to v0.16
- Updated AV-8B Harrier 2 to v0.61
- Updated F-16 Falcon to v1.986
- Updated F-14 Tomcat to v1.99
- Updated A-10 Warthog v1.83a
- Updated FIR AWS US to v3.54
- Updated RHS: AFRF to v0.5.4
- Updated RHS: USAF to 0.5.4
- Updated RHS: GREF to 0.5.4
- Updated RHS: SAF to 0.5.4
- Updated JSRS to CE.20.0810
- Updated VCOM AI to V3.4.0


**08JUL2020**
- Updated BWI Addons to 08JUL2020.
- Updated CBA to v3.15.1.200418.
- Updated ACE Custom Build to v3.13.2.
- Updated ACEX to v3.5.2.
- Updated ACRE2 to v2.7.4.1027.
- Updated CUP Terrains Core to v1.15.0.
- Updated CUP Terrains Maps to v1.14.0.
- Updated Redd 'n' Tanks to v1.12.172.
- Updated Theseus Services to v1.14.1.
- Updated F-15 Eagle to v1.52.
- Updated Lingor to v3.9.6.01.


**22MAR2020**
- Updated ACE Custom Build to v1.13.1.


**20MAR2020**
- Updated BWI Addons to 20MAR2020.


**14MAR2020**
- Updated BWI Addons to 14MAR2020.


**13MAR2020**
- Updated BWI Addons to 13MAR2020.


**09MAR2020**
- Updated BWI Addons to 09MAR2020.


**08MAR2020**
- Updated BWI Addons to 08MAR2020.


**06MAR2020**
- Added new terrains:
  - Kujari
  - Suursaari
  - Vinjesvingen
  - Virolahti
- Updated BWI Addons to 06MAR2020.
- Updated CBA to v3.14.0.200207.
- Updated ACE Custom Build to v3.13.0.
- Updated ACEX to v3.5.0.
- Updated ACRE2 to v2.7.2.1022.
- Updated CUP Terrains Core to v1.13.0.
- Updated CUP Terrains Maps to v1.13.0.
- Updated RHS: AFRF to v0.5.0.
- Updated RHS: USAF to v0.5.0.
- Updated RHS: GREF to v0.5.0.
- Updated RHS: SAF to v0.5.0.
- Updated 3CB BAF Units to v4.10.
- Updated 3CB BAF Vehicles to v10.0.
- Updated 3CB BAF Weapons to v3.0.
- Updated 3CB Factions to v2.0.2.
- Updated Redd 'n' Tanks to v1.12.162.
- Updated Theseus Services to v1.12.0.
- Updated JSRS to v19.1119.
- Updated FIR AWS to v3.23.
- Updated F-14 Tomcat to v1.97.
- Updated F-15 Eagle to v1.51.
- Updated Rosche to v2.0.
- Updated Hellanmaa to v1.03.
- Updated Pulau to v0.05.
- Updated Ruha to v0.12.
- Removed ArmaReTex FV510 Warrior.
- Removed old terrains:
  - Aliabad Region
  - GOS Dariyah
  - Hazar Kot Valley
  - Ihantala
  - Summa


**19DEC2019**
- Updated BWI Addons to 19DEC2019.
- Updated ACE Medical Rewrite to v3.12.6.54.


**17OCT2019**
- Updated BWI Addons to 17OCT2019.
- Updated CBA to v3.12.2.190909.
- Updated ACRE2 to v2.7.1.1016.
- Updated BWMod to v2.2.
- Updated Redd'n'Tank to v1.11.157.
- Updated Theseus Services to v1.11.02.
- Updated ShackTac UI to v1.2.5.02.
- Updated Anizay to v0.08.
- Updated Hellanmaa to v1.02.
- Updated Summa to v0.05.
- Updated FIR AWS to v3.0a.
- Updated F-16 Fighting Falcon to v1.97a.
- Updated F-14 Tomcat to v1.95.
- Updated F-15 Eagle to v1.48.
- Updated A-10 Warthog to v1.7.
- Updated AV-8B Harrier to v0.53.
- Updated Su-25SM3 Grach to v0.14.


**02MAY2019**
- Updated BWI Addons to 02MAY2019.


**26APR2019**
- Added VCOM AI.
- Added 3CB Factions.
- Added Su-25SM3 Grach.
- Added F-35B Lightning.
- Added Redd 'n' Tank Vehicles.
- Added ArmaReTex FV510 Warrior.
- Added new terrains:
  - Aliabad Region
  - Anizay
  - Dariyah
  - Fallujah
  - Hazar-Kot Valley
  - Hellanmaa
  - Ihantala
  - Pulau
  - Rosche
  - Ruha
  - Summa
- Updated BWI Addons to 26APR2019.
- Updated CBA to v3.10.1.190316.
- Updated ACE Medical Rewrite to v3.12.6.
- Updated ACEX to v3.4.2.
- Updated ACRE to v2.6.2.996.
- Updated CUP Terrains Core to v1.12.0.
- Updated CUP Terrains Maps to v1.12.0.
- Updated RHS: AFRF to v0.4.8.
- Updated RHS: USAF to v0.4.8.
- Updated RHS: GREF to v0.4.8.
- Updated RHS: SAF to v0.4.8.
- Updated 3CB BAF Units to v4.9.
- Updated 3CB BAF Weapons to v2.5.
- Updated 3CB BAF Equipment to v2.6.
- Updated 3CB BAF Vehicles to v9.0.
- Updated BWMod to v2.1.1.
- Updated Theseus Services to v1.8.0.
- Updated FIR AWS to v2.87.
- Updated F-16 Fighting Falcon Series Standalone to v1.96.
- Updated F-14 Tomcat Series Standalone to v1.91.
- Updated F-15 Eagle Series Standalone to v1.47.
- Updated A-10 Warthog to v1.62.
- Updated AV-8B Harrier to v0.51a.
- Updated F/A-18 Super Hornet to v5.1.
- Updated Su-35S Flanker E to v3.0.0.
- Updated Lingor to v3.9.5.
- Updated Isla Duala to v3.9.5.
- Updated JSRS Soundmod to v6.19.0130.
- Updated ShackTac User Interface to v1.2.4.
- Removed NIArms Arsenal.
- Removed TalibanFighters.
- Removed RDS A2 Civilian Pack.
- Removed old terrains:
  - Chernarus Winter
  - Imrali Island
  - Leskovets
  - N'Djenahoud
  - Porquerolles
  - Prei Khmaoch Luong
  - Song Bin Tanh
  - VT5
  - Wake Island


**02AUG2018**
- Added FEMAL3.
- Added F/A-18 Super Hornet.
- Added Su-35S Flanker E.
- Added AV-8B Harrier.
- Updated CBA to v3.7.1.180604.
- Updated ACE to v3.12.2.
- Updated 3CB BAF Vehicles to v7.0.
- Updated Theseus Services to v1.4.0.
- Updated ShackTac UI to v1.2.3.
- Updated NIArms Arsenal to v10.
- Updated FIR AWS to v2.54.
- Updated F-16 Fighting Falcon Series Standalone to v1.93.
- Updated F-14 Tomcat Series Standalone to v1.74.
- Updated F-15 Eagle Series Standalone to v1.33.
- Updated A-10 Warthog to v1.44.
- Updated N'Djenahoud to v1.01.
- Updated Wake Island to v1.2.
- Updated JSRS Soundmod to v6.18.0718B (optional).
- Removed Eurofighter Typhoon AWS.


**14MAR2018**
- Updated BWI Addons to 14MAR2018.
- Updated CBA to v3.6.1.180131.
- Updated ACE to v3.12.1.
- Updated ACEX to v3.3.0.
- Updated CUP Terrains Core to v1.4.2.
- Updated CUP Terrains Maps to v1.4.2.
- Updated RHS: AFRF to v0.4.5.
- Updated RHS: USAF to v0.4.5.
- Updated RHS: GREF to v0.4.5.
- Updated RHS: SAF to v0.4.5.
- Updated BWMod to v1.6.
- Updated NIArms Arsenal to v9.3.
- Updated FIR AWS to v2.27.
- Updated F-16 Fighting Falcon Series Standalone to v1.81.
- Updated F-14 Tomcat Series Standalone to v1.68.
- Updated F-15 Eagle Series Standalone to v1.21.
- Updated A-10 Warthog to v1.31.
- Updated Panthera to v3.9.
- Updated Isla Abramia to v3.9.
- Updated Isla Duala to v3.9.
- Updated JSRS Soundmod to v6.17.1222 (optional).


**23DEC2017**
- Updated BWI Addons to 23DEC2017.


**09DEC2017**
- Updated BWI Addons to 09DEC2017.
- Updated CBA to v3.5.0.171204.
- Updated ACRE2 to v2.5.1.980.
- Updated CUP Terrains Core to v1.4.0.
- Updated CUP Terrains Maps to v1.4.0.
- Updated Isla Abramia to v1.9.
- Updated JSRS Soundmod v6.17.1206 (optional).


**02DEC2017**
- Updated BWI Addons to 02DEC2017.
- Updated BWMod ACE Compatibility to v1.5.2.


**01DEC2017**
- Updated BWMod to v1.5.2 (Hotfix)


**30NOV2017**
- Added RHS: SAF v0.4.4.
- Added NIArms NIArsenal v8.0.
- Updated BWI Addons to 30NOV2017.
- Updated ACE to v3.11.0.
- Updated RHS: AFRF to v0.4.4.
- Updated RHS: USAF to v0.4.4.
- Updated RHS: GREF to v0.4.4.
- Updated FIR AWS to v2.18.
- Updated F-16 Fighting Falcon Series Standalone to v1.79.
- Updated F-14 Tomcat Series Standalone to v1.62.
- Updated F-15 Eagle Series Standalone to v1.12.
- Updated A-10 Warthog to v1.2.
- Updated JSRS Soundmod to v6.17.1110 (optional).
- Removed Panavia Tornado AWS.
- Removed TF47 Launchers.
- Removed CUP Terrains CWA Maps.
- Removed Diyala.


**10OCT2017**
- Updated BWI Addons to 10OCT2017.
- Updated CBA to v3.4.1.170912.
- Updated ACRE2 to v2.5.0.968.
- Updated 3CB BAF Units to v4.2.
- Updated 3CB BAF Weapons to v2.2.1.
- Updated 3CB BAF Equipment to v2.2.
- Updated 3CB BAF Vehicles to v6.1.
- Updated FIR AWS to v2.16.
- Updated F-16 Fighting Falcon Series Standalone to v1.78.
- Updated F-14 Tomcat Series Standalone to v1.61.
- Updated F-15 Eagle Series Standalone to v1.1.
- Updated A-10 Warthog to v1.14.


**12SEP2017**
- Updated BWI Addons to 12SEP2017.
- Updated Theseus Services to v1.3.0.
- Updated FIR AWS to v2.1.
- Updated JSRS Soundmod to v5.17.0807 (optional).


**31JUL2017**
- Added Prei Khmaoch Luong.
- Updated BWI Addons to 31JUL2017.
- Updated ACE to v3.10.2.
- Updated Bozcaada to v1.2.


**11JUL2017**
- Updated BWI Addons to 11JUL2017.
- Updated CBA to v3.4.0.170627.
- Updated FIR AWS to v2.0.
- Updated F-16 Fighting Falcon Series Standalone to v1.77.
- Updated F-14 Tomcat Series Standalone to v1.5.
- Updated F-15 Eagle Series Standalone to v1.01.
- Updated A-10 Warthog to v1.13.
- Updated JSRS Soundmod to v5.17.0705 (optional).


**26JUN2017**
- Added new terrains:
  - Leskovets
  - N'Djenahoud
  - Song Bin Tanh
- Updated BWI Addons to 26JUN2017.
- Updated ACE to v3.10.1.
- Updated FIR AWS to v1.76.
- Updated F-16 Fighting Falcon Series Standalone to v1.75.
- Updated A-10 Warthog to v1.1.
- Updated JSRS Soundmod to v5.17.0619 (optional).


**07JUN2017**
- Updated BWI Addons to 07JUN2017.
- Updated FIR AWS to v1.74.
- Updated F-16 Fighting Falcon Series Standalone to v1.74.
- Updated A-10 Warthog to v1.0.
- Updated JSRS Soundmod to v5.17.0524 (optional).


**18MAY2017**
- Updated ACE to v3.9.2.
- Updated ACRE2 to v2.4.2.954.
- Updated BWI Addons to 18MAY2017.
- Updated BWMod to v1.5.1.
- Updated CBA to v3.3.0.170504.
- Updated FIR AWS to v1.7.
- Updated F-16 Fighting Falcon Series Standalone to v1.7.
- Updated Theseus Services to v1.2.1.
- Updated JSRS Soundmod to v5.17.0.517 (optional).


**17APR2017**
- Updated BWI Addons to 17APR2017.
- Updated RHS: AFRF to v0.4.2.2.
- Updated RHS: USAF to v0.4.2.2.
- Updated RHS: GREF to v0.4.2.2.
- Updated 3CB BAF Units to v4.1.
- Updated 3CB BAF Weapons to v2.1.
- Updated 3CB BAF Equipment to v2.1.
- Updated 3CB BAF Vehicles to v6.0.
- Updated BWMod to v1.5.
- Updated ACEX to v3.2.0.
- Updated ShackTac UI to v1.2.2.
- Updated Lingor to v3.82.
- Updated Diyala to v1.3.02.
- Updated A-10 Warthog to beta 5.
- Updated BackpackOnChest to v1.2.0.


**26MAR2017**
- Updated BWI Addons to 26MAR2017.
- Updated ACE to v3.9.1.
- Updated ACRE2 to v2.4.1.947.
- Updated Eurofighter Typhoon AWS to v1.55.


**05MAR2017**
- Updated BWI Addons to 05MAR2017.
- Removed Kunduz.
 

**28FEB2017**
- Added ACRE2.
- Added NIArms Core.
- Added NIArms AK Rifles.
- Added NIArms AR15 Rifles.
- Added NIArms FAL Rifles.
- Added NIArms G3 Rifles.
- Added NIArms G36 Rifles.
- Added NIArms M14 Rifles.
- Added NIArms M60 GPMGs.
- Added NIArms MG3 GPMGs.
- Added NIArms MP5 SMGs.
- Added TF47 Launchers.
- Added A-10 Warthog.
- Added Kunduz.
- Updated CBA to v3.2.0.1702224.
- Updated ACE to v3.9.0.
- Updated RHS: AFRF to v0.4.2.1.
- Updated RHS: USAF to v0.4.2.1.
- Updated RHS: GREF to v0.4.2.1.
- Updated CUP Terrains to v1.3.0.
- Updated Lingor to v3.81.
- Updated Isla Duala to v3.8.
- Updated Isla Abramia to v1.8.
- Updated Island Panthera to v3.7.
- Updated BWMod to v1.4.1.
- Updated FIR AWS to v1.55.
- Updated F-14 Tomcat Series Standalone to v1.1.
- Updated F-15 Eagle Series Standalone to v0.6.
- Updated F-16 Fighting Falcon Series Standalone to v1.4.
- Updated Theseus Services to v1.2.0.
- Updated Backpack on Chest to v1.1.1.
- Updated JSRS Soundmod to v5.17.0215 (optional).
- Removed Angel Island.
- Removed Helvantis.
- Removed Kerama Islands.
- Removed Task Force Arrowhead Radio.
- Removed VME Chinese PLA.


**16DEC2016**
- Updated BWI Addons to 16DEC2016.


**27OCT2016**
- Updated BWI Addons to 27OCT2016.
- Updated CBA to v3.1.1.161012.
- Updated ACE to v3.8.1.
- Updated ACEX to v3.1.1.


**16OCT2016**
- Updated BWI Addons to 16OCT2016.


**14OCT2016**
- Added 3CB BAF Weapons.
- Added 3CB BAF Equipment.
- Added 3CB BAF Vehicles.
- Added 3CB BAF Units.
- Added BWMod.
- Added VME Chinese PLA.
- Added Theseus Services.
- Added RDS A2 Civilian Pack.
- Added F-15 Eagle Series Standalone.
- Added Panavia Tornado AWS.
- Added Eurofighter Typhoon AWS.
- Added new maps:
  - Lingor
  - Isla Abramia
  - Isla Duala
  - Island Panthera
  - Bozcaada Island
  - Helvantis
  - Diyala
  - Kerama Islands
  - Porquerolles
  - Valtatie 5 (VT5)
  - Imrali Islands
  - Al Rayak
  - Angel Island
  - Chernarus Winter
  - Wake Island
  - Gunkizli
- Updated ACE to v3.7.0.
- Updated CBA to v3.1.0.160928.
- Updated ShackTac User Interface to v1.2.0.2.
- Updated FIR AWS to v1.3.
- Updated F-16C Fighting Falcon Standalone to v1.31.
- Updated Taliban Fighter to RC20.


**06SEP2016**
- Added FIR AWS US.
- Added F-14 Tomcat Series Standalone.
- Added F-16 Fighting Falcon Series Standalone.


**19AUG2016**
- Added CBA.
- Added ACE.
- Added ACEX.
- Added RHS: AFRF.
- Added RHS: USAF.
- Added RHS: GREF.
- Added CUP Terrains.
- Added BWI Addons.
- Added ShackTac Fireteam HUD.
- Added BackpackOnChest.
- Added Taliban Fighters.
- Added Task Force Arrowhead Radio.
- Added JSRS4 Apex (optional).
